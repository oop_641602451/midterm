public class Animal {
    private String type;
    private String name;
    private int age ;

    public Animal(String type, String name, int age) {
        this.type = type;
        this.name = name;
        this.age = age;
    }
    public void print() {
        System.out.println("type: " + type + " name: " + name + " age: " + age);
    }
    public String getType() {
        return type;
    }
    public String getName() {
        return name;
    }
    public int getAge() {
        return age;
    }

    public static void main(String[] args) {
        Animal น้ำหวาน = new Animal("cat", "Nahwan", 9);
        Animal เต่า = new Animal("turtle", "turtle", 6);
        น้ำหวาน.print();
        เต่า.print();
    }
}
