public class Food {
    private String type;
    private String name;
    private int amount;

    public Food(String type, String name, int amount) {
        this.type = type;
        this.name = name;
        this.amount = amount;
    }
    public void print() {
        System.out.println("type: " + type + " name: " + name + " amount: " + amount);
    }
    public String getType() {
        return type;
    }
    public String getName() {
        return name;
    }
    public int getAmount() {
        return amount;
    }

    public static void main(String[] args) {
        Food watermelon = new Food("fruit", "watermelon", 1);
        watermelon.print();
    }
}
